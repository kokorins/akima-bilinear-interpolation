package akima;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Akima {
    private static double Spdt(List<Double> x1, List<Double> x2, List<Double> x3) {
        return (x1.get(0)-x2.get(0))*(x3.get(0)-x2.get(0))+(x1.get(1)-x2.get(1))*(x3.get(1)-x2.get(1));
    }

    private static double Side(List<Double> x1, List<Double> x2, List<Double> x3) {
        return (x3.get(1)-x1.get(1))*(x2.get(0)-x1.get(0))-(x3.get(0)-x1.get(0))*(x2.get(1)-x1.get(1));
    }

    private static double SideCnt(List<Double> x1, List<Double> x2, List<Double> x3) {
        return (x1.get(0)-x3.get(0))*(x2.get(1)-x3.get(1)) - (x1.get(1)-x3.get(1))*(x2.get(0)-x3.get(0));
    }

    private static double Side(double u1, double v1, double u2, double v2, double u3, double v3) {
        return (v3-v1)*(u2-u1)-(u3-u1)*(v2-v1);
    }

    private static double SqrDist(List<Double> x, List<Double> y) {
        double sum =0;
        for(int i=0; i<x.size(); ++i)
            sum += Math.pow(x.get(i)-y.get(i),2);
        return sum;
    }

    static class TriangulationData {
        int numTrian;
        List<int[]> ipt; //INTEGER ARRAY OF DIMENSION 6*NDP-15, WHERE THE
                   //   POINT NUMBERS OF THE VERTEXES OF THE (IT)TH
                   //   TRIANGLE ARE TO BE STORED AS THE (3*IT-2)ND,
                   //  (3*IT-1)ST, AND (3*IT)TH ELEMENTS, IT=1,2,...,NT,
        int numLines;
        List<int[]> ipl;  // INTEGER ARRAY OF DIMENSION 6*NDP, WHERE THE
                    // POINT NUMBERS OF THE END POINTS OF THE (IL)TH
                    // BORDER LINE SEGMENT AND ITS RESPECTIVE TRIANGLE
                    // NUMBER ARE TO BE STORED AS THE (3*IL-2)ND,
                    // (3*IL-1)ST, AND (3*IL)TH ELEMENTS,
                    // IL=1,2,..., NL.
    }

    public static Pair<Integer, Integer> MinDist(List<List<Double>> x) {
        int ipmn1 = 0;
        int ipmn2 = 1;
        double dsqmn = SqrDist(x.get(ipmn1), x.get(ipmn2));
        for(int ip1 = 0; ip1<x.size()-1; ++ip1) {
            List<Double> x1 = x.get(ip1);
            for(int ip2=ip1+1; ip2<x.size(); ++ip2) {
                double dsqi = SqrDist(x1, x.get(ip2));
                if(dsqi<Double.MIN_VALUE)
                    throw new RuntimeException("Duplication");
                if(dsqmn<=dsqi)
                    continue;
                dsqmn = dsqi;
                ipmn1 = ip1;
                ipmn2 = ip2;
            }
        }
        return Pair.of(ipmn1, ipmn2);
    }

    public static TriangulationData IdTang(final List<List<Double>> x) {
        List<int[]> ipt = new ArrayList<>(2*x.size()-5);
        List<int[]> ipl = new ArrayList<>(2*x.size());
        int[] iwl = new int[18*x.size()];
        double ratio = 1e-6;

        Pair<Integer, Integer> minIdxs = MinDist(x);
        int ipmn1 = minIdxs.getLeft();
        int ipmn2 = minIdxs.getRight();
        final List<Integer> iwp = InitIWP(x, ipmn1, ipmn2);
        // IF NECESSARY, MODIFIES THE ORDERING IN SUCH A WAY THAT THE
        // FIRST THREE DATA POINTS ARE NOT COLLINEAR.
        double dsqmn = SqrDist(x.get(ipmn1), x.get(ipmn2));
        double ar = dsqmn*ratio;
        int jp = CheckColinearity(x, ipmn1, ipmn2, iwp, ar);
        if(jp == x.size())
            throw new RuntimeException("Collinear points present in x");
        if(jp!=2) {
            Collections.swap(iwp, 2, jp);
        } // 40
        // FORMS THE FIRST TRIANGLE.  STORES POINT NUMBERS OF THE VERTEXES
        // OF THE TRIANGLE IN THE IPT ARRAY, AND STORES POINT NUMBER
        // OF THE BORDER LINE SEGMENTS AND THE TRIANGLE NUMBER IN THE IPL ARRAY.
        if(Side(x.get(iwp.get(0)), x.get(iwp.get(1)), x.get(iwp.get(2)))<0) {
            Collections.swap(iwp, 0, 1);
        }
        ipt.add(new int[]{iwp.get(0), iwp.get(1), iwp.get(2)});
        ipl.add(new int[]{iwp.get(0), iwp.get(1), 0});
        ipl.add(new int[]{iwp.get(1), iwp.get(2), 0});
        ipl.add(new int[]{iwp.get(2), iwp.get(0), 0});
        // ADDS THE REMAINING (NDP-3) DATA POINTS, ONE BY ONE.
        Goto79: for(int jp1 = 3; jp1<x.size(); ++jp1) {
            int ip1 = iwp.get(jp1);
            List<Double> x1 = x.get(ip1);
            int ip2 = ipl.get(0)[0];
            Pair<Integer, Integer> jpmm = JpMinMax(x, ipl, ratio, x1, ip2);
            int jpmn = jpmm.getLeft();
            int jpmx = jpmm.getRight();
            int nsh = jpmn-1;
            if(nsh>=0) {
                // SHIFTS (ROTATES) THE IPL ARRAY TO HAVE THE INVISIBLE BORDER
                // LINE SEGMENTS CONTAINED IN THE FIRST PART OF THE IPL ARRAY.
                for(int j=0; j<nsh+1; ++j) {
                    ipl.add(ipl.remove(0));
                }
                jpmx -= nsh+1;
            }
            // ADDS TRIANGLES TO THE IPT ARRAY, UPDATES BORDER LINE
            // SEGMENTS IN THE IPL ARRAY, AND SETS FLAGS FOR THE BORDER
            // LINE SEGMENTS TO BE REEXAMINED IN THE IWL ARRAY.
            int jwl = 0;
            int nln = ipl.size();
            for(int jp2 = jpmx; jp2<nln; ++jp2) { // 60
                int ipl1 = ipl.get(jp2)[0];
                int ipl2 = ipl.get(jp2)[1];
                int it = ipl.get(jp2)[2];
                // ADDS A TRIANGLE TO THE IPT ARRAY.
                ipt.add(new int[]{ipl2, ipl1, ip1});
                // UPDATES BORDER LINE SEGMENTS IN THE IPL ARRAY.
                if(jp2 == jpmx) {
                    ipl.get(jp2)[1] = ip1;
                    ipl.get(jp2)[2] = ipt.size()-1;
                }
                if(jp2+1 == nln) { // 61
                    if(jpmx+1 == ipl.size())
                        ipl.add(new int[]{ip1, ipl.get(0)[0], ipt.size()-1});
                    else
                        ipl.set(jpmx+1, new int[]{ip1, ipl.get(0)[0], ipt.size()-1});
                }
                //DETERMINES THE VERTEX THAT DOES NOT LIE ON THE BORDER LINE SEGMENTS.
                int ipti = ipt.get(it)[0];
                if(ipti==ipl1 || ipti == ipl2) {
                    ipti = ipt.get(it)[1];
                }
                if(ipti == ipl1 || ipti == ipl2) {
                    ipti = ipt.get(it)[2];
                }
                // CHECKS IF THE EXCHANGE IS NECESSARY.
                if(IdxChg(x, ip1, ipti, ipl1, ipl2)) { // 63
                    ipt.set(it, new int[] {ipti, ipl1, ip1});
                    ipt.get(ipt.size()-1)[1] = ipti;
                    if(jp2==jpmx)
                        ipl.get(jp2)[2]=it;
                    if(jp2+1 == nln && ipl.get(0)[2] == it)
                        ipl.get(0)[2] = ipt.size()-1;
//                  SETS FLAGS IN THE IWL ARRAY.
                    jwl += 4;
                    iwl[jwl-4] = ipl1;
                    iwl[jwl-3] = ipti;
                    iwl[jwl-2] = ipti;
                    iwl[jwl-1] = ipl2;
                }
            } // 64
            int nlf = jwl/2;
            if(nlf==0)
                continue;
            // IMPROVES TRIANGULATION.
            final int nrep = 100;
            for(int irep = 1; irep<=nrep; ++irep) {
                for(int ilf=1; ilf<=nlf; ++ilf) {
                    int ilft2 = ilf*2;
                    int ipl1 = iwl[ilft2-1-1];
                    int ipl2 = iwl[ilft2-1];
                    // LOCATES IN THE IPT ARRAY TWO TRIANGLES ON BOTH SIDES OF
                    // THE FLAGGED LINE SEGMENT.
                    List<Integer> itf = new ArrayList<>();
                    for(int itt = 0; itt<ipt.size(); ++itt) {
                        int itt3 = ipt.size() - itt - 1;
                        int ipt1 = ipt.get(itt3)[0];
                        int ipt2 = ipt.get(itt3)[1];
                        int ipt3 = ipt.get(itt3)[2];
                        if(ipl1 != ipt1 && ipl1!=ipt2 && ipl1 != ipt3)
                            continue;
                        if(ipl2 != ipt1 && ipl2!=ipt2 && ipl2 != ipt3)
                            continue;
                        itf.add(itt3);
                        if(itf.size()==2)
                            break;
                    }
                    if(itf.size()<2)
                        continue;
                    int it1t3 = itf.get(0);
                    int ipti1 = ipt.get(it1t3)[0];
                    if(ipti1 == ipl1 || ipti1 == ipl2)
                        ipti1 = ipt.get(it1t3)[1];
                    if(ipti1 == ipl1 || ipti1 == ipl2)
                        ipti1 = ipt.get(it1t3)[2];
                    int it2t3 = itf.get(1);
                    int ipti2 = ipt.get(it2t3)[0];
                    if(ipti2 == ipl1 || ipti2 == ipl2)
                        ipti2 = ipt.get(it2t3)[1];
                    if(ipti2 == ipl1 || ipti2 == ipl2)
                        ipti2 = ipt.get(it2t3)[2];
                    if(IdxChg(x, ipti1, ipti2, ipl1, ipl2)) { // 74
                        ipt.set(it1t3, new int[]{ipti1, ipti2, ipl1});
                        ipt.set(it2t3, new int[]{ipti2, ipti1, ipl2});
                        // SETS NEW FLAGS.
                        jwl += 8;
                        iwl[jwl-8]=ipl1;
                        iwl[jwl-7]=ipti1;
                        iwl[jwl-6]=ipti1;
                        iwl[jwl-5]=ipl2;
                        iwl[jwl-4]=ipl2;
                        iwl[jwl-3]=ipti2;
                        iwl[jwl-2]=ipti2;
                        iwl[jwl-1]=ipl1;
                        for(int jl=0; jl<ipl.size(); ++jl) {
                            int iplj1=ipl.get(jl)[0];
                            int iplj2=ipl.get(jl)[1];
                            if((iplj1==ipl1 && iplj2  == ipti2)
                                    || (iplj2== ipl1 && iplj1 == ipti2))
                                ipl.get(jl)[2] = itf.get(0);
                            if((iplj1==ipl2 && iplj2==ipti1) ||
                                    (iplj2==ipl2 && iplj1==ipti1))
                                ipl.get(jl)[2] = itf.get(1);
                        }
                    } // Exchange
                }
                int nlfc = nlf;
                nlf = jwl/2;
                if(nlf==nlfc)
                    continue Goto79; //MUAHAHA 2
                // RESETS THE IWL ARRAY FOR THE NEXT ROUND.
                jwl = 0;
                int jwl1mn = (nlfc+1)*2;
                int nlft2 = nlf*2;
                for(int jwl1 = jwl1mn; jwl1<=nlft2; jwl1+=2) {
                    jwl+=2;
                    iwl[jwl-1-1]=iwl[jwl1-1-1];
                    iwl[jwl-1] = iwl[jwl1-1];
                }
                nlf = jwl/2;
            } // 78
        } // 79
        // REARRANGES THE IPT ARRAY SO THAT THE VERTEXES OF EACH TRIANGLE
        // ARE LISTED COUNTER-CLOCKWISE.
        for(int it=0; it<ipt.size(); ++it) {
            int ip1 = ipt.get(it)[0];
            int ip2 = ipt.get(it)[1];
            int ip3 = ipt.get(it)[2];
            if(Side(x.get(ip1), x.get(ip2), x.get(ip3))<0) {
                ipt.get(it)[0] = ip2;
                ipt.get(it)[1] = ip1;
            }
        }
        TriangulationData td = new TriangulationData();
        td.ipl = ipl;
        td.ipt = ipt;
        td.numLines = ipl.size();
        td.numTrian = ipt.size();
        return td;
    }

    private static Pair<Integer, Integer> JpMinMax(List<List<Double>> x, List<int[]> ipl, double ratio,
                                                   List<Double> x1, int ip2) {
        double dsqmn;
        double ar;//DETERMINES THE VISIBLE BORDER LINE SEGMENTS.
        List<Double> dMin = new ArrayList<>();
        for(int i=0; i<x1.size(); ++i) {
            dMin.add(x.get(ip2).get(i)-x1.get(i));
        }
        dsqmn = SqrDist(x.get(ip2), x1);
        double armn = dsqmn*ratio;
        List<Double> dMax = new ArrayList<>(dMin);
        double dsqMax = dsqmn;
        double arMax = armn;
        int jpmn = 0, jpmx = 0;
        for(int jp2=1; jp2<ipl.size(); ++jp2) { // 52
            ip2 = ipl.get(jp2)[0];
            List<Double> d = new ArrayList<>();
            double dsqi = SqrDist(x.get(ip2), x1);
            for(int j=0; j<x1.size(); ++j) {
                d.add(x.get(ip2).get(j)-x1.get(j));
            }
            ar = d.get(1)*dMin.get(0)-d.get(0)*dMin.get(1);
            if(ar<=armn) {
                if((ar<-armn)||dsqi<dsqmn) {
                    jpmn = jp2;
                    dMin = new ArrayList<>(d);
                    dsqmn = dsqi;
                    armn = dsqmn*ratio;
                }
            }
            ar = d.get(1)*dMax.get(0)-d.get(0)*dMax.get(1); // 51
            if(ar>=-arMax) {
                if(ar>arMax || dsqi<dsqMax) {
                    jpmx = jp2;
                    dMax=d;
                    dsqMax = dsqi;
                    arMax = dsqMax*ratio;
                }
            }
        }

        if(jpmx<jpmn)
            jpmx+=ipl.size();
        return new ImmutablePair<>(jpmn, jpmx);
    }

    private static int CheckColinearity(List<List<Double>> x, int ipmn1, int ipmn2, List<Integer> iwp, double ar) {
        List<Double> dxMin = new ArrayList<>();
        for(int i=0; i<x.get(ipmn1).size(); ++i)
            dxMin.add(x.get(ipmn2).get(i)-x.get(ipmn1).get(i));
        int jp;
        for(jp=2; jp<iwp.size(); ++jp) {
            int ip = iwp.get(jp);
            double eps = (x.get(ip).get(1)-x.get(ipmn1).get(1))*dxMin.get(0)
                            - (x.get(ip).get(0)-x.get(ipmn1).get(0))*dxMin.get(1);
            if(Math.abs(eps)>ar)
                break;
        }
        return jp;
    }

    private static List<Integer> InitIWP(final List<List<Double>> x, int ipmn1, int ipmn2) {
        final List<Double> midPoint = new ArrayList<>();
        for(int i=0; i<x.get(ipmn1).size(); ++i)
            midPoint.add((x.get(ipmn1).get(i)+x.get(ipmn2).get(i))/2);
        // SORTS THE OTHER (NDP-2) DATA POINTS IN ASCENDING ORDER OF
        // DISTANCE FROM THE MIDPOINT AND STORES THE SORTED DATA POINT
        // NUMBERS IN THE IWP ARRAY.
        List<Integer> iwp = new ArrayList<>();
        for(int i=0; i<x.size(); ++i)
            iwp.add(i);
        Collections.sort(iwp, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhsIdx, Integer rhsIdx) {
                return Double.compare(SqrDist(midPoint, x.get(lhsIdx)), SqrDist(midPoint, x.get(rhsIdx)));
            }
        });
        return iwp;
    }

    /**
     * C THIS FUNCTION DETERMINES WHETHER OR NOT THE EXCHANGE OF TWO
     * TRIANGLES IS NECESSARY ON THE BASIS OF MAX-MIN-ANGLE CRITERION
     * BY C. L. LAWSON.
     * @param x ARRAYS CONTAINING THE COORDINATES OF THE DATA POINTS,
     * @param i1 I1,I2,I3,I4 = POINT NUMBERS OF FOUR POINTS P1, P2,
     *          P3, AND P4 THAT FORM A QUADRILATERAL WITH P3
     *           AND P4 CONNECTED DIAGONALLY.
     * @return THIS FUNCTION RETURNS AN INTEGER VALUE 1 (ONE) WHEN AN EX-
     * CHANGE IS NECESSARY, AND 0 (ZERO) OTHERWISE.
     */
    private static boolean IdxChg(List<List<Double>> x, int i1, int i2, int i3, int i4) {
        double x1 = x.get(i1).get(0);
        double y1 = x.get(i1).get(1);
        double x2 = x.get(i2).get(0);
        double y2 = x.get(i2).get(1);
        double x3 = x.get(i3).get(0);
        double y3 = x.get(i3).get(1);
        double x4 = x.get(i4).get(0);
        double y4 = x.get(i4).get(1);
        double u3 = (y2-y3)*(x1-x3)-(x2-x3)*(y1-y3);
        double u4 = (y1-y4)*(x2-x4)-(x1-x4)*(y2-y4);
        if(u3*u4<=0)
            return false;
        double u1=(y3-y1)*(x4-x1)-(x3-x1)*(y4-y1);
        double u2=(y4-y2)*(x3-x2)-(x4-x2)*(y3-y2);
        double A1SQ=Math.pow((x1-x3),2)+Math.pow((y1-y3),2);
        double B1SQ=Math.pow((x4-x1),2)+Math.pow((y4-y1),2);
        double C1SQ=Math.pow((x3-x4),2)+Math.pow((y3-y4),2);
        double C2SQ = C1SQ;
        double A2SQ=Math.pow((x2-x4),2)+Math.pow((y2-y4),2);
        double B2SQ=Math.pow((x3-x2),2)+Math.pow((y3-y2),2);
        double C3SQ=Math.pow((x2-x1),2)+Math.pow((y2-y1),2);
        double C4SQ = C3SQ;
        double S1SQ=u1*u1/(C1SQ*Math.max(A1SQ,B1SQ));
        double S2SQ=u2*u2/(C2SQ*Math.max(A2SQ,B2SQ));
        double A3SQ = B2SQ;
        double B3SQ = A1SQ;
        double S3SQ=u3*u3/(C3SQ*Math.max(A3SQ,B3SQ));
        double A4SQ = B1SQ;
        double B4SQ = A2SQ;
        double S4SQ=u4*u4/(C4SQ*Math.max(A4SQ,B4SQ));
        return Math.min(S1SQ, S2SQ)<Math.min(S3SQ, S4SQ);
    }

    static class CoordComparator implements Comparator<List<Double>> {
        private final int _dim;

        CoordComparator(int dim) {
            _dim = dim;
        }

        @Override
        public int compare(List<Double> lhs, List<Double> rhs) {
            return Double.compare(lhs.get(_dim), rhs.get(_dim));
        }
    }
    /**
     * C THIS SUBROUTINE LOCATES A POINT, I.E., DETERMINES TO WHAT TRI-
     * C ANGLE A GIVEN POINT (XII,YII) BELONGS.  WHEN THE GIVEN POINT
     * C DOES NOT LIE INSIDE THE DATA AREA, THIS SUBROUTINE DETERMINES
     * C THE BORDER LINE SEGMENT WHEN THE POINT LIES IN AN OUTSIDE
     * C RECTANGULAR AREA, AND TWO BORDER LINE SEGMENTS WHEN THE POINT
     * C LIES IN AN OUTSIDE TRIANGULAR AREA.
     */
    public static int IdlCnt(List<Double> at, List<List<Double>> x, TriangulationData td) {
        int[] iwk = new int[18*x.size()];
        double[] wk = new double[8*x.size()];
        int nt0 = td.numTrian;
        int nl0 = td.numLines;
        int ntl = nt0+nl0;
        double xmn = Collections.min(x, new CoordComparator(0)).get(0);
        double xmx = Collections.max(x, new CoordComparator(0)).get(0);
        double ymn = Collections.min(x, new CoordComparator(1)).get(1);
        double ymx = Collections.max(x, new CoordComparator(1)).get(1);
        double xs1 = (2*xmn+xmx)/3.;
        double xs2 = (xmn+2*xmx)/3.;
        double ys1 = (2*ymn+ymx)/3.;
        double ys2 = (ymn+2*ymx)/3.;
        // DETERMINES AND STORES IN THE IWK ARRAY TRIANGLE NUMBERS OF
        // THE TRIANGLES ASSOCIATED WITH EACH OF THE NINE SECTIONS.
        final int kTsc = 9;
        int[] ntsc = new int[kTsc];
        int[] idsc = new int[kTsc];
        for(int isc = 0; isc<kTsc; ++isc) {
            ntsc[isc] = 0;
            idsc[isc] = 0;
        }
        int jwk = 0;
        for(int it0=0; it0<nt0; ++it0) {
            int i1 = td.ipt.get(it0)[0];
            int i2 = td.ipt.get(it0)[1];
            int i3 = td.ipt.get(it0)[2];
            xmn = Math.min(x.get(i1).get(0), Math.min(x.get(i2).get(0), x.get(i3).get(0)));
            xmx = Math.max(x.get(i1).get(0), Math.max(x.get(i2).get(0), x.get(i3).get(0)));
            ymn = Math.min(x.get(i1).get(1), Math.min(x.get(i2).get(1), x.get(i3).get(1)));
            ymx = Math.max(x.get(i1).get(1), Math.max(x.get(i2).get(1), x.get(i3).get(1)));
            if(ymn<=ys1) {
                if(xmn<=xs1)
                    idsc[0] = 1;
                if(xmx>=xs1 && xmn<=xs2)
                    idsc[1] = 1;
                if(xmx>=xs2)
                    idsc[2] = 1;
            }
            if(ymx>=ys1 && ymn<=ys2) {
                if(xmn<=xs1)
                    idsc[3] = 1;
                if(xmx>=xs1 && xmn<=xs2)
                    idsc[4] = 1;
                if(xmx>=xs2)
                    idsc[5] = 1;
            }
            if(ymx>=ys2) {
                if(xmn<=xs1)
                    idsc[6] = 1;
                if(xmx>=xs1 && xmn<=xs2)
                    idsc[7] = 1;
                if(xmx>=xs2)
                    idsc[8] = 1;
            }
            for(int isc=0; isc<kTsc; ++isc) {
                if(idsc[isc]==0)
                    continue;
                int jiwk = 9*ntsc[isc]+isc;
                iwk[jiwk] = it0;
                ntsc[isc] = ntsc[isc]+1;
                idsc[isc] = 0;
            }//60
            // STORES IN THE WK ARRAY THE MINIMUM AND MAXIMUM OF THE X AND
            // Y COORDINATE VALUES FOR EACH OF THE TRIANGLE.
            jwk += 4;
            wk[jwk-4] = xmn;
            wk[jwk-3] = xmx;
            wk[jwk-2] = ymn;
            wk[jwk-1] = ymx;
        } // 70
        // 110
        // LOCATES INSIDE THE DATA AREA.
        // DETERMINES THE SECTION IN WHICH THE POINT IN QUESTION LIES.
        int isc = 0;
        if(at.get(0)>=xs1)
            ++isc;
        if(at.get(0)>=xs2)
            ++isc;
        if(at.get(1)>=ys1)
            isc +=3;
        if(at.get(1)>=ys2)
            isc +=3;
        // SEARCHES THROUGH THE TRIANGLES ASSOCIATED WITH THE SECTION.
        int ntsci = ntsc[isc];
        if(ntsci>0) {
            int  jiwk = -9+isc;
            for(int itsc = 0; itsc<ntsci; ++itsc) {
                jiwk += 9;
                int it0 = iwk[jiwk];
                jwk =(it0+1)*4-1;
                if(at.get(0)<wk[jwk-3])
                    continue;
                if(at.get(0)>wk[jwk-2])
                    continue;
                if(at.get(1)<wk[jwk-1])
                    continue;
                if(at.get(1)>wk[jwk])
                    continue;
                int ip1 = td.ipt.get(it0)[0];
                List<Double> x1 = x.get(ip1);
                int ip2 = td.ipt.get(it0)[1];
                List<Double> x2 = x.get(ip2);
                if(SideCnt(x1, x2, at)<0)
                    continue;
                int ip3 = td.ipt.get(it0)[2];
                List<Double> x3 = x.get(ip3);
                if(SideCnt(x2, x3, at)<0)
                    continue;
                if(SideCnt(x3, x1, at)<0)
                    continue;
                return it0;
            }//120
            // LOCATES OUTSIDE THE DATA AREA.
            for(int il1=0; il1<nl0; ++il1) {
                int ip1 = td.ipl.get(il1)[0];
                List<Double> x1 = x.get(ip1);
                int ip2 = td.ipl.get(il1)[1];
                List<Double> x2 = x.get(ip2);
                if(Spdt(x2, x1, at)<0)
                    continue;
                if(Spdt(x1, x2, at)<0) { //goto 140
                    int il2 = il1%nl0;
                    int ip3 = td.ipl.get(il2)[2];
                    List<Double> x3 = x.get(ip3);
                    if(Spdt(x3, x2, at)<0)
                        return il1*ntl+il2;
                    continue;
                }
                if(SideCnt(x1, x2, at)>0)
                    continue;
                int il2 = il1;
                return il1*ntl+il2;
            }//150
            return 0;
        }
        return 0;
    }

    /**
     * THIS SUBROUTINE PERFORMS LINEAR PUNCTUAL INTERPOLATION,
     * I.E., DETERMINES THE Z VALUE AT A POINT.
     * @param at point to make interpolation at
     * @param x points of a grid
     * @param z 3rd coordinate of grid
     * @param td Triangulation information
     * @param iti TRIANGLE NUMBER OF THE TRIANGLE IN WHICH LIES
     *          THE POINT FOR WHICH INTERPOLATION IS TO BE PERFORMED,
     * @return INTERPOLATED Z VALUE.
     */
    public static double IdpTli(List<Double> at, List<List<Double>> x,
                                List<Double> z, TriangulationData td, int iti) {
        int ntl = td.numLines + td.numTrian;
        if(iti>=ntl)
            return Double.NaN;
        // CALCULATION OF ZII BY INTERPOLATION.
        // CHECKS IF THE NECESSARY COEFFICIENTS HAVE BEEN CALCULATED.
//        if(iti!=itpv)
        // LOADS COORDINATE AND PARTIAL DERIVATIVE VALUES AT THE VERTEXES.
        List<List<Double>> x3 = new ArrayList<>();
        List<Double> z3 = new ArrayList<>();
        for(int i=0; i<3; ++i) {
            int idp = td.ipt.get(iti)[i];
            x3.add(x.get(idp));
            z3.add(z.get(idp));
        }
        // DETERMINES THE COEFFICIENTS FOR THE COORDINATE SYSTEM
        // TRANSFORMATION FROM THE X-Y SYSTEM TO THE U-V SYSTEM
        // AND VICE VERSA.
        double x0 = x3.get(0).get(0);
        double y0 = x3.get(0).get(1);
        double a = x3.get(1).get(0)-x0;
        double b = x3.get(2).get(0)-x0;
        double c = x3.get(1).get(1)-y0;
        double d = x3.get(2).get(1)-y0;
        double ad = a*d;
        double bc = b*c;
        double dlt = ad-bc;
        double ap = d/dlt;
        double bp = -b/dlt;
        double cp = -c/dlt;
        double dp = a/dlt;
        // CONVERTS XII AND YII TO U-V SYSTEM.
        double dx = at.get(0)-x0;
        double dy = at.get(1)-y0;
        double u = ap*dx+bp*dy;
        double v = cp*dx+dp*dy;
        // EVALUATES THE INTERPOLATED PLANE
        // ACCORDING TO
        //  |  U   V  ZII-Z1 |
        //  |  1   0   Z2-Z1 | = 0
        //  |  0   1   Z3-Z1 |
        return z3.get(0)+u*(z3.get(1)-z3.get(0))+v*(z3.get(2)-z3.get(0));
    }

    /**
     * C THIS SUBROUTINE PERFORMS BIVARIATE INTERPOLATION WHEN THE PRO-
     * C JECTIONS OF THE DATA POINTS IN THE X-Y PLANE ARE IRREGULARLY
     * C DISTRIBUTED IN THE PLANE.
     * CALLS THE IDCLDP, IDLCTN, IDPDRV, IDPTIP, AND IDTANG
     * @param at points to be interpolated at
     * @param x  points to work with
     * @param z  points of plane where the interpolation should be done
     * @return interpolated points
     */
    public static List<Double> Interpolate(List<List<Double>> at, List<List<Double>> x, List<Double> z) {
        if(x.size()<4)
            throw new RuntimeException("Need at least 4 points");
        TriangulationData triangulationData = IdTang(x);
        if(triangulationData.numTrian<1)
            throw new RuntimeException("Incorrect triangulation");
        List<Integer> idxs = new ArrayList<>();
        for(List<Double> a : at)
            idxs.add(IdlCnt(a, x, triangulationData));
        // INTERPOLATES THE ZI VALUES
        List<Double> res = new ArrayList<>();
        for(int i=0; i<at.size(); ++i)
            res.add(IdpTli(at.get(i), x, z, triangulationData, idxs.get(i)));
        return res;
    }
}
