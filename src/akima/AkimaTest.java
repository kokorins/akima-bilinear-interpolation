package akima;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class AkimaTest {
    @Test
    public void testSmokeInterpolate() throws Exception {
        List<Double> res = Akima.Interpolate(Arrays.asList(Arrays.asList(0.5, 0.5)),
                Arrays.asList(Arrays.asList(0., 0.),
                        Arrays.asList(0., 1.),
                        Arrays.asList(1., 0.),
                        Arrays.asList(1., 1.)),
                Arrays.asList(0., 0., 0., 1.));
        Assert.assertEquals(1, res.size());
        double exp = 0;
        Assert.assertEquals(exp, res.get(0), 1e-5);
    }

    @Test
    public void testInterpolate() throws Exception {
        List<Double> res = Akima.Interpolate(Arrays.asList(Arrays.asList(0.5, 0.5)),
                Arrays.asList(Arrays.asList(0., 0.),
                        Arrays.asList(0., 1.),
                        Arrays.asList(1., 0.),
                        Arrays.asList(1., 1.)),
                Arrays.asList(0., 1., 2., 3.));
        Assert.assertEquals(1, res.size());
        double exp = 1.5;
        Assert.assertEquals(exp, res.get(0), 1e-5);
    }

    @Test
    public void testInterpolateNonSymm() throws Exception {
        List<Double> res = Akima.Interpolate(Arrays.asList(Arrays.asList(0.2, 0.8)),
                Arrays.asList(Arrays.asList(0., 0.),
                        Arrays.asList(0., 1.),
                        Arrays.asList(1., 0.),
                        Arrays.asList(1., 1.)),
                Arrays.asList(0., 1., 2., 3.));
        Assert.assertEquals(1, res.size());
        double exp = 1.2;
        Assert.assertEquals(exp, res.get(0), 1e-5);
    }


    @Test
    public void testIdTang() throws Exception {
        Akima.TriangulationData td = Akima.IdTang(Arrays.asList(Arrays.asList(0., 0.),
                                                                Arrays.asList(0., 1.),
                                                                Arrays.asList(1., 0.),
                                                                Arrays.asList(1., 1.)));
        Assert.assertEquals(2, td.numTrian);
        Assert.assertEquals(td.numTrian, td.ipt.size());
        List<int[]> iptExp = Arrays.asList(new int[]{1, 0, 2}, new int[]{1, 2, 3});
        List<int[]> iplExp = Arrays.asList(new int[]{1, 0, 0},
                                           new int[]{0, 2, 0},
                                           new int[]{2, 3, 1},
                                           new int[]{3, 1, 1});
        for (int i1 = 0; i1 < iptExp.size(); i1++) {
            int[] expTrian = iptExp.get(i1);
            for (int i = 0; i < expTrian.length; ++i)
                Assert.assertEquals(expTrian[i], td.ipt.get(i1)[i]);
        }
        Assert.assertEquals(4, td.numLines);
        Assert.assertEquals(td.numLines, td.ipl.size());
        for (int i1 = 0; i1 < iplExp.size(); i1++) {
            int[] expLine = iplExp.get(i1);
            for (int i = 0; i < expLine.length; ++i)
                Assert.assertEquals(expLine[i], td.ipl.get(i1)[i]);
        }
    }

    @Test
    public void testIdlCntNonSymm() throws Exception {
        List<Double> at = Arrays.asList(0.2, 0.8);
        List<List<Double>> x = Arrays.asList(Arrays.asList(0., 0.),
                Arrays.asList(0., 1.),
                Arrays.asList(1., 0.),
                Arrays.asList(1., 1.));
        Akima.TriangulationData td = new Akima.TriangulationData();
        td.numTrian = 2;
        td.numLines = 4;
        td.ipl = Arrays.asList(new int[]{1, 0, 0},
                new int[]{0, 2, 0},
                new int[]{2, 3, 1},
                new int[]{3, 1, 1});
        td.ipt = Arrays.asList(new int[]{1, 0, 2}, new int[]{1, 2, 3});
        int iti = Akima.IdlCnt(at, x, td);
        Assert.assertEquals(1, iti);
    }

    @Test
    public void testIdlCnt() throws Exception {
        List<Double> at = Arrays.asList(0.5, 0.5);
        List<List<Double>> x = Arrays.asList(Arrays.asList(0., 0.),
                                             Arrays.asList(0., 1.),
                                             Arrays.asList(1., 0.),
                                             Arrays.asList(1., 1.));
        Akima.TriangulationData td = new Akima.TriangulationData();
        td.numTrian = 2;
        td.numLines = 4;
        td.ipl = Arrays.asList(new int[]{1, 0, 0},
                new int[]{0, 2, 0},
                new int[]{2, 3, 1},
                new int[]{3, 1, 1});
        td.ipt = Arrays.asList(new int[]{1, 0, 2}, new int[]{1, 2, 3});
        int i = Akima.IdlCnt(at, x, td);
        Assert.assertEquals(0, i);
    }

    @Test
    public void testIdlTli() throws Exception {
        List<Double> at = Arrays.asList(0.5, 0.5);
        List<List<Double>> x = Arrays.asList(Arrays.asList(0., 0.),
                Arrays.asList(0., 1.),
                Arrays.asList(1., 0.),
                Arrays.asList(1., 1.));
        Akima.TriangulationData td = new Akima.TriangulationData();
        td.numTrian = 2;
        td.numLines = 4;
        td.ipl = Arrays.asList(new int[]{1, 0, 0},
                new int[]{0, 2, 0},
                new int[]{2, 3, 1},
                new int[]{3, 1, 1});
        td.ipt = Arrays.asList(new int[]{1, 0, 2}, new int[]{1, 2, 3});
        int iti = 0;
        List<Double> z = Arrays.asList(0., 1., 2., 3.);
        double i = Akima.IdpTli(at, x, z, td, iti);
        Assert.assertEquals(1.5, i, 1e-5);
    }

    @Test
    public void testIdlTliNonSymm() throws Exception {
        List<Double> at = Arrays.asList(0.2, 0.8);
        List<List<Double>> x = Arrays.asList(Arrays.asList(0., 0.),
                Arrays.asList(0., 1.),
                Arrays.asList(1., 0.),
                Arrays.asList(1., 1.));
        Akima.TriangulationData td = new Akima.TriangulationData();
        td.numTrian = 2;
        td.numLines = 4;
        td.ipl = Arrays.asList(new int[]{1, 0, 0},
                new int[]{0, 2, 0},
                new int[]{2, 3, 1},
                new int[]{3, 1, 1});
        td.ipt = Arrays.asList(new int[]{1, 0, 2}, new int[]{1, 2, 3});
        int iti = 1;
        List<Double> z = Arrays.asList(0., 1., 2., 3.);
        double val = Akima.IdpTli(at, x, z, td, iti);
        Assert.assertEquals(1.2, val, 1e-5);
    }


    @Test
    public void testInterpolateMultiple() throws Exception {
        List<Double> res = Akima.Interpolate(Arrays.asList(Arrays.asList(0.5, 0.5),
                                                           Arrays.asList(0.2, 0.8),
                                                           Arrays.asList(0.8, 0.2),
                                                           Arrays.asList(0.0, 1.0)),
                Arrays.asList(Arrays.asList(0., 0.),
                        Arrays.asList(0., 1.),
                        Arrays.asList(1., 0.),
                        Arrays.asList(1., 1.)),
                Arrays.asList(0., 1., 2., 3.));
        List<Double> exp = Arrays.asList(1.5, 1.2, 1.8, 1.0);
        Assert.assertEquals(exp.size(), res.size());
        for(int i=0; i<exp.size(); ++i)
            Assert.assertEquals(exp.get(i), res.get(i), 1e-5);
    }

    @Test
    public void testNonSymmetric() throws Exception {
        List<Double> res = Akima.Interpolate(Arrays.asList(Arrays.asList(0.8, 0.2)),
                Arrays.asList(Arrays.asList(0., -0.1),
                              Arrays.asList(0., 0.9),
                              Arrays.asList(1., 0.1),
                              Arrays.asList(1.1, 1.)),
                Arrays.asList(0., 1., 2., 3.));
        List<Double> exp = Arrays.asList(1.74);
        Assert.assertEquals(exp.size(), res.size());
        for(int i=0; i<exp.size(); ++i)
            Assert.assertEquals(exp.get(i), res.get(i), 1e-5);
    }


    @Test
    public void testNonSymmetricMultiple() throws Exception {
        List<Double> res = Akima.Interpolate(Arrays.asList(Arrays.asList(0.5, 0.5),
                                                           Arrays.asList(0.2, 0.8),
                                                           Arrays.asList(0.8, 0.2),
                                                           Arrays.asList(0.0, 0.8)),
                Arrays.asList(Arrays.asList(0., -0.1),
                              Arrays.asList(0., 0.9),
                              Arrays.asList(1., 0.1),
                              Arrays.asList(1.1, 1.)),
                Arrays.asList(0., 1., 2., 3.));
        List<Double> exp = Arrays.asList(1.5, 1.255102, 1.74, 0.9);
        Assert.assertEquals(exp.size(), res.size());
        for(int i=0; i<exp.size(); ++i)
            Assert.assertEquals(exp.get(i), res.get(i), 1e-5);
    }

    @Test
    public void testBiggerNonSymmetricMultiple() throws Exception {
        List<Double> res = Akima.Interpolate(Arrays.asList(Arrays.asList(0.5, 0.5),
                                                           Arrays.asList(0.2, 0.8),
                                                           Arrays.asList(0.8, 0.2),
                                                           Arrays.asList(0.0, 0.8)),
                Arrays.asList(Arrays.asList(0., -0.1),
                              Arrays.asList(0., 0.9),
                              Arrays.asList(1., 0.1),
                              Arrays.asList(0.4, 0.6),
                              Arrays.asList(2.2, 1.5),
                              Arrays.asList(0.1, 0.6),
                              Arrays.asList(0.7, 0.4)),
                Arrays.asList(0., 1., 2., 3., 2.0, 4.0, 7.0));
        List<Double> exp = Arrays.asList(4.413793, 1.777778, 3.777778, 0.9);
        Assert.assertEquals(exp.size(), res.size());
        for(int i=0; i<exp.size(); ++i)
            Assert.assertEquals(exp.get(i), res.get(i), 1e-5);
    }

    @Test
    public void testBiggerIdTang() throws Exception {
        Akima.TriangulationData td = Akima.IdTang(
                Arrays.asList(Arrays.asList(0., -0.1),
                              Arrays.asList(0., 0.9),
                              Arrays.asList(1., 0.1),
                              Arrays.asList(0.4, 0.6),
                              Arrays.asList(2.2, 1.5),
                              Arrays.asList(0.1, 0.6),
                              Arrays.asList(0.7, 0.4)));
        Assert.assertEquals(td.numTrian, td.ipt.size());
        Assert.assertEquals(td.numLines, td.ipl.size());

        List<int[]> iptExp = Arrays.asList(new int[]{5, 3, 1},
                                           new int[]{3, 5, 0},
                                           new int[]{3, 6, 4},
                                           new int[]{5, 1, 0},
                                           new int[]{6, 3, 0},
                                           new int[]{6, 0, 2},
                                           new int[]{6, 2, 4},
                                           new int[]{1, 3, 4});

        List<int[]> iplExp = Arrays.asList(new int[]{1, 0, 3},
                                           new int[]{0, 2, 5},
                                           new int[]{2, 4, 6},
                                           new int[]{4, 1, 7});
        Assert.assertEquals("Incorrect number of triangles", iptExp.size(), td.ipt.size());
        Assert.assertEquals("Incorrect number of lines", iplExp.size(), td.ipl.size());

        for (int i1 = 0; i1 < iptExp.size(); i1++) {
            int[] expTrian = iptExp.get(i1);
            for (int i = 0; i < expTrian.length; ++i)
                Assert.assertEquals(expTrian[i], td.ipt.get(i1)[i]);
        }

        for (int i1 = 0; i1 < iplExp.size(); i1++) {
            int[] expLine = iplExp.get(i1);
            for (int i = 0; i < expLine.length; ++i)
                Assert.assertEquals(expLine[i], td.ipl.get(i1)[i]);
        }
    }

    @Test
    public void testIdTang2() throws Exception {
        List<List<Double>> x = Arrays.asList(Arrays.asList(0.,0.), Arrays.asList(0.,1.), Arrays.asList(0.,2.),
                                             Arrays.asList(1.,0.),                       Arrays.asList(1.,2.),
                                             Arrays.asList(2.,0.), Arrays.asList(2.,1.), Arrays.asList(2.,2.));
        Akima.TriangulationData td = Akima.IdTang(x);
        List<int[]> iptExp = Arrays.asList(new int[]{1, 0, 3,},
                                           new int[]{1, 3, 4,},
                                           new int[]{2, 1, 4,},
                                           new int[]{3, 5, 6,},
                                           new int[]{4, 3, 6,},
                                           new int[]{4, 6, 7,});
        List<int[]> iplExp = Arrays.asList(new int[]{4, 2, 2,},
                                           new int[]{2, 1, 2,},
                                           new int[]{1, 0, 0,},
                                           new int[]{0, 3, 0,},
                                           new int[]{3, 5, 3,},
                                           new int[]{5, 6, 3,},
                                           new int[]{6, 7, 5,},
                                           new int[]{7, 4, 5,});
        Assert.assertEquals("Incorrect number of triangles", iptExp.size(), td.ipt.size());
        Assert.assertEquals("Incorrect number of lines", iplExp.size(), td.ipl.size());
        for (int i1 = 0; i1 < iptExp.size(); i1++) {
            int[] expTrian = iptExp.get(i1);
            for (int i = 0; i < expTrian.length; ++i)
                Assert.assertEquals(expTrian[i], td.ipt.get(i1)[i]);
        }

        for (int i1 = 0; i1 < iplExp.size(); i1++) {
            int[] expLine = iplExp.get(i1);
            for (int i = 0; i < expLine.length; ++i)
                Assert.assertEquals(expLine[i], td.ipl.get(i1)[i]);
        }
    }

    @Test
    public void testOneMore(){
        List<List<Double>> at = Arrays.asList(Arrays.asList(2.,0.5));
        List<List<Double>> x = Arrays.asList(Arrays.asList(0.,0.), Arrays.asList(0.,1.), Arrays.asList(0.,2.),
                                             Arrays.asList(1.,0.),                       Arrays.asList(1.,2.),
                                             Arrays.asList(2.,0.), Arrays.asList(2.,1.), Arrays.asList(2.,2.));
        List<Double> z = Arrays.asList(0.,0.,0.,0.,1.,0.,1.,1.);
        List<Double> interpolate = Akima.Interpolate(at, x, z);
        Assert.assertEquals(1, interpolate.size());
        Assert.assertEquals(0.5, interpolate.get(0));
    }

    @Test
    public void testEdgeCase() {
        List<List<Double>> at = Arrays.asList(Arrays.asList(-0.1,0.));
        List<List<Double>> x = Arrays.asList(Arrays.asList(0.,1.), Arrays.asList(1.,1.), Arrays.asList(1., 0.), Arrays.asList(0.9, 0.9));
        List<Double> z = Arrays.asList(1., 2., 1., 1.8);
        List<Double> interpolate = Akima.Interpolate(at, x, z);
        Assert.assertEquals(1, interpolate.size());
        Assert.assertEquals(-0.1, interpolate.get(0), 1e-5);
    }
}
